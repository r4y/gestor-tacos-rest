package com.ray.gestor.tacos.excepciones;

import com.ray.gestor.tacos.utils.ObjetoInvalido;

public class CampoArregloExcepcion extends RuntimeException {
	private ObjetoInvalido objetoInvalido;
	
	public CampoArregloExcepcion(ObjetoInvalido objetoInvalido, Exception ex) {
		super(ex);
		this.objetoInvalido = objetoInvalido;
	}
	
	public CampoArregloExcepcion(ObjetoInvalido objetoInvalido) {
		this.objetoInvalido = objetoInvalido;
	}
	
	public ObjetoInvalido getObjetoInvalido() {
		return objetoInvalido;
	}

	public void setObjetoInvalido(ObjetoInvalido objetoInvalido) {
		this.objetoInvalido = objetoInvalido;
	}

	private static final long serialVersionUID = 1L;

}
