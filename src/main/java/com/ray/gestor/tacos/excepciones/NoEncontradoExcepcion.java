package com.ray.gestor.tacos.excepciones;

public class NoEncontradoExcepcion extends RuntimeException{
	private String mensajeError;
	
	public NoEncontradoExcepcion(String mensajeError, Exception ex) {
		super(ex);
		this.mensajeError = mensajeError;
	}
	
	public NoEncontradoExcepcion(String mensajeError) {
		this.mensajeError = mensajeError;
	}

	public String getMensajeError() {
		return mensajeError;
	}

	public void setMensajeError(String mensajeError) {
		this.mensajeError = mensajeError;
	}

	private static final long serialVersionUID = 1L;

}
