package com.ray.gestor.tacos;

import java.util.TimeZone;

import javax.annotation.PostConstruct;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GestorTacosRestApplication {
	
	@PostConstruct
	public void init() {
		TimeZone.setDefault(TimeZone.getTimeZone("America/Monterrey"));
	}
	
	public static void main(String[] args) {
		SpringApplication.run(GestorTacosRestApplication.class, args);
	}

}

