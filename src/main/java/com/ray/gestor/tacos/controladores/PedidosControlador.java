package com.ray.gestor.tacos.controladores;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.ray.gestor.tacos.modelo.entidades.Pedido;
import com.ray.gestor.tacos.modelo.servicios.GestorService;

@RestController
@RequestMapping("/ordenes")
public class PedidosControlador {
	@Autowired
	GestorService servicio;
	
	@RequestMapping(value="/{ordenId}/pedidos")
	public List<Pedido> getPedidos(@PathVariable Long ordenId) {
		return servicio.obtenerPedidosPorOrdenId(ordenId);
	}
	
	@RequestMapping(value="/{ordenId}/pedidos", method= {RequestMethod.POST})
	@ResponseStatus(code = HttpStatus.CREATED)
	public List<Pedido> agregarPedido(@PathVariable Long ordenId, @Valid @RequestBody List<Pedido> nuevosPedidos) {
		return servicio.agregarPedidos(ordenId, nuevosPedidos); 
	}
}
