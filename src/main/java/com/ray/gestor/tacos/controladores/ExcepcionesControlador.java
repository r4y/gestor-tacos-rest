package com.ray.gestor.tacos.controladores;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.ray.gestor.tacos.excepciones.CampoArregloExcepcion;
import com.ray.gestor.tacos.excepciones.NoEncontradoExcepcion;
import com.ray.gestor.tacos.utils.RespuestaError;
import com.ray.gestor.tacos.utils.ObjetoInvalido;

@ControllerAdvice
public class ExcepcionesControlador extends ResponseEntityExceptionHandler {

	@ExceptionHandler(value = NoEncontradoExcepcion.class)
	public ResponseEntity<RespuestaError<String>> noEncontradoException(NoEncontradoExcepcion ex) {
		return new ResponseEntity<RespuestaError<String>>(
				new RespuestaError<String>(HttpStatus.NOT_FOUND, ex.getMensajeError(), "Tu corazón ;3"),
				HttpStatus.NOT_FOUND);
	}

	@ExceptionHandler(value = CampoArregloExcepcion.class)
	public ResponseEntity<RespuestaError<ObjetoInvalido>> requestMalHecho(CampoArregloExcepcion ex) {
		return new ResponseEntity<RespuestaError<ObjetoInvalido>>(
				new RespuestaError<ObjetoInvalido>(HttpStatus.BAD_REQUEST, ex.getObjetoInvalido(), "Tu corazón ;3 <3!"),
				HttpStatus.BAD_REQUEST);
	}
}
