package com.ray.gestor.tacos.controladores;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ray.gestor.tacos.modelo.entidades.Producto;
import com.ray.gestor.tacos.modelo.servicios.GestorService;

@RestController
@RequestMapping("/productos")
public class ProductosControlador {

	@Autowired
	GestorService servicio;
	
	@GetMapping("/")
	public List<Producto> getProductos() {
		return servicio.getProductos();
	}
	
	@GetMapping("/{idProducto}")
	public Producto getProducto(@PathVariable Long idProducto) {
		return servicio.obtenerProductoPorID(idProducto);
	}
	
	@PostMapping("/")
	public Producto agregarProducto(@Valid @RequestBody Producto nuevoProducto) {
		return servicio.putProducto(nuevoProducto);
	}
}
