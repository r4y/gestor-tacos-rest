package com.ray.gestor.tacos.controladores;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.ray.gestor.tacos.modelo.entidades.Orden;
import com.ray.gestor.tacos.modelo.servicios.GestorService;

@RestController
@RequestMapping("/ordenes")
public class OrdenesControlador {
	@Autowired
	GestorService servicio;
	
	@GetMapping("/")
	public List<Orden> obtenerOrdenes() {
		List<Orden> ordenesEnviar = servicio.obtenerOrdenes();
		return ordenesEnviar;
	}
	
	@GetMapping("/{ordenId}")
	public Orden obtenerOrden(@PathVariable Long ordenId) {
		Orden ordenEnviar = servicio.obtenerOrdenPorId(ordenId);
		return ordenEnviar;
	}
	
	@PostMapping("/")
	@ResponseStatus(code = HttpStatus.CREATED)
	public Orden agregarOrden(@Valid @RequestBody Orden nuevaOrden) {
		Orden ordenAEnviar = servicio.agregarOrden(nuevaOrden);
		return ordenAEnviar;
	}
}
