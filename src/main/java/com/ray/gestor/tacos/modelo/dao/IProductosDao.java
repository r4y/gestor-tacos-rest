package com.ray.gestor.tacos.modelo.dao;

import org.springframework.data.repository.CrudRepository;

import com.ray.gestor.tacos.modelo.entidades.Producto;

public interface IProductosDao extends CrudRepository<Producto, Long> {
	
}
