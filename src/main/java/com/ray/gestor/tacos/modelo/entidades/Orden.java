package com.ray.gestor.tacos.modelo.entidades;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Entity
@Table(name="ordenes")
public class Orden implements Serializable {
	//TODO: Agregar estatus de orden, mientras todos los pedidos sean COMPLETADOS, cambiar a COMPLETADO
	//TODO: Calcular el total de toda la orden
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@NotNull
	private Integer mesa;

	@Column(name = "fecha_orden")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaOrden;
	
	@Column(name="total_orden")
	private Double totalOrden;

	@OneToMany(mappedBy="orden", fetch = FetchType.LAZY)
	@Valid
	@NotEmpty
	@NotNull
	private List<Pedido> pedidos;

	@PrePersist
	public void prePersist() {
		fechaOrden = new Timestamp(System.currentTimeMillis());
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getMesa() {
		return mesa;
	}

	public void setMesa(Integer mesa) {
		this.mesa = mesa;
	}

	public Date getFechaOrden() {
		return fechaOrden;
	}

	public void setFechaOrden(Date fechaOrden) {
		this.fechaOrden = fechaOrden;
	}

	public List<Pedido> getPedidos() {
		return pedidos;
	}

	public void setPedidos(List<Pedido> pedidos) {
		this.pedidos = pedidos;
	}

	public boolean addPedido(Pedido nuevoPedido) {
		return pedidos.add(nuevoPedido);
	}

	public Double getTotalOrden() {
		return totalOrden;
	}

	public void setTotalOrden(Double totalOrden) {
		this.totalOrden = totalOrden;
	}
	
	public void acumularTotalOrden(Double cantidad) {
		if(totalOrden == null) {
			totalOrden = cantidad;
		}
		else {
			totalOrden += cantidad;
		}
	}

	@Override
	public String toString() {
		return "Orden [id=" + id + ", mesa=" + mesa + ", fechaOrden=" + fechaOrden + ", totalOrden=" + totalOrden
				+ ", pedidos=" + pedidos + "]";
	}

}
