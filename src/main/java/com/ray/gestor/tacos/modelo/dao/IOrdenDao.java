package com.ray.gestor.tacos.modelo.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import com.ray.gestor.tacos.modelo.entidades.Orden;

@Service
public interface IOrdenDao extends CrudRepository<Orden, Long> {

}
