package com.ray.gestor.tacos.modelo.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.ray.gestor.tacos.modelo.entidades.Pedido;

public interface IPedidosDao extends CrudRepository<Pedido, Long> {
	public List<Pedido> findByOrdenId(Long ordenId);

}
