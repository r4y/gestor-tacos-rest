package com.ray.gestor.tacos.modelo.entidades;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="pedidos")
public class Pedido implements Serializable {
	//TODO: Calcular el total de cada pedido
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@NotNull
	@Column(name="cantidad_producto")
	private Integer cantidadProducto;
	
	@NotNull
	@Column(name="estatus_pedido")
	private String estatusPedido;
	
	@Column(name="total_pedido")
	private Double totalPedido;
	
	@ManyToOne
	@NotNull
	@JoinColumn(name="producto_id")
	private Producto productoPedido;
	
	@ManyToOne
	@JsonIgnore
	@JoinColumn(name="orden_id")
	private Orden orden;

	
	public Orden getOrden() {
		return orden;
	}

	public void setOrden(Orden orden) {
		this.orden = orden;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getCantidadProducto() {
		return cantidadProducto;
	}

	public void setCantidadProducto(Integer cantidadProducto) {
		this.cantidadProducto = cantidadProducto;
	}

	public String getEstatusPedido() {
		return estatusPedido;
	}

	public void setEstatusPedido(String estatusPedido) {
		this.estatusPedido = estatusPedido;
	}

	public Producto getProductoPedido() {
		return productoPedido;
	}

	public void setProductoPedido(Producto productoPedido) {
		this.productoPedido = productoPedido;
	}
	
	public Double getTotalPedido() {
		return totalPedido;
	}

	public void setTotalPedido(Double totalPedido) {
		this.totalPedido = totalPedido;
	}

	@Override
	public String toString() {
		return "Pedido [id=" + id + ", cantidadProducto=" + cantidadProducto + ", estatusPedido=" + estatusPedido
				+ ", totalPedido=" + totalPedido + ", productoPedido=" + productoPedido + "]";
	}


}
