package com.ray.gestor.tacos.modelo.servicios;

import java.util.List;

import javax.transaction.Transactional;
import javax.validation.ConstraintViolationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ray.gestor.tacos.excepciones.CampoArregloExcepcion;
import com.ray.gestor.tacos.excepciones.NoEncontradoExcepcion;
import com.ray.gestor.tacos.modelo.dao.IOrdenDao;
import com.ray.gestor.tacos.modelo.dao.IPedidosDao;
import com.ray.gestor.tacos.modelo.dao.IProductosDao;
import com.ray.gestor.tacos.modelo.entidades.Orden;
import com.ray.gestor.tacos.modelo.entidades.Pedido;
import com.ray.gestor.tacos.modelo.entidades.Producto;
import com.ray.gestor.tacos.utils.ObjetoInvalido;

@Service
public class GestorService {
	@Autowired
	IProductosDao productoDao;
	@Autowired
	IPedidosDao pedidosDao;
	@Autowired
	IOrdenDao ordenDao;

	public List<Producto> getProductos() {
		return (List<Producto>) productoDao.findAll();
	}

	public Producto obtenerProductoPorID(Long id) {
		Producto productoEncontrado = productoDao.findById(id).orElse(null);
		if (productoEncontrado == null) {
			throw new NoEncontradoExcepcion("Producto con el id = '" + id + "' no encontrado.");
		}
		return productoEncontrado;
	}

	public Producto putProducto(Producto nuevoProducto) {
		return productoDao.save(nuevoProducto);
	}

	public List<Pedido> obtenerPedidos() {
		return (List<Pedido>) pedidosDao.findAll();
	}

	public Pedido agregarPedido(Pedido nuevoPedido) {
		Pedido nuevoPedidoGuardado = pedidosDao.save(nuevoPedido);
		Long idProductoNuevo = nuevoPedidoGuardado.getProductoPedido().getId();
		nuevoPedidoGuardado.setProductoPedido(obtenerProductoPorID(idProductoNuevo));
		return nuevoPedidoGuardado;
	}

	public List<Orden> obtenerOrdenes() {
		return (List<Orden>) ordenDao.findAll();
	}
	
	@Transactional(rollbackOn = ConstraintViolationException.class)
	public Orden agregarOrden(Orden nuevaOrden) {
		Producto productoPedido;
		Orden nuevaOrdenGuardada = ordenDao.save(nuevaOrden);
		
		try {
			for (Pedido pedido : nuevaOrdenGuardada.getPedidos()) {
				productoPedido = obtenerProductoPorID(pedido.getProductoPedido().getId());
				pedido.setOrden(nuevaOrdenGuardada);
				pedido.setTotalPedido(productoPedido.getPrecioUnitario() * pedido.getCantidadProducto());
				pedido = agregarPedido(pedido);
				nuevaOrdenGuardada.acumularTotalOrden(pedido.getTotalPedido());
			}
		} catch (ConstraintViolationException e) {
			ObjetoInvalido pedidoInvalido = new ObjetoInvalido(Pedido.class.getSimpleName(), e);
			CampoArregloExcepcion nuevaExcepcion = new CampoArregloExcepcion(pedidoInvalido, e);
			throw nuevaExcepcion;
		}
		
		return nuevaOrdenGuardada;
	}

	@Transactional(rollbackOn = ConstraintViolationException.class)
	public List<Pedido> agregarPedidos(long ordenId, List<Pedido> nuevosPedidos) {
		Producto productoPedido;
		Orden ordenModificar = obtenerOrdenPorId(ordenId);
		try {
			for (Pedido pedido : nuevosPedidos) {
				productoPedido = obtenerProductoPorID(pedido.getProductoPedido().getId());
				pedido.setOrden(ordenModificar);
				pedido.setTotalPedido(productoPedido.getPrecioUnitario() * pedido.getCantidadProducto());
				pedido = agregarPedido(pedido);
				ordenModificar.acumularTotalOrden(pedido.getTotalPedido());
			}
		} catch (ConstraintViolationException e) {
			ObjetoInvalido pedidoInvalido = new ObjetoInvalido(Pedido.class.getSimpleName(), e);
			CampoArregloExcepcion nuevaExcepcion = new CampoArregloExcepcion(pedidoInvalido, e);
			throw nuevaExcepcion;
		}
		return nuevosPedidos;
	}

	public List<Pedido> obtenerPedidosPorOrdenId(Long ordenId) {
		List<Pedido> pedidosEncontrados = pedidosDao.findByOrdenId(ordenId);
		if (pedidosEncontrados.isEmpty()) {
			throw new NoEncontradoExcepcion("No se encontraron pedidos de la orden con id = '" + ordenId + "'");
		}
		System.out.println("pedidosEncontrados = " + pedidosEncontrados);
		return pedidosEncontrados;
	}

	public Orden obtenerOrdenPorId(Long id) {
		Orden ordenEncontrada = ordenDao.findById(id).orElse(null);
		if (ordenEncontrada == null) {
			throw new NoEncontradoExcepcion("Orden con id = '" + id + "' no encontrado.");
		}
		return ordenEncontrada;
	}

}
