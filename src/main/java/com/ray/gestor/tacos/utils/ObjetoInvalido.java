package com.ray.gestor.tacos.utils;

import java.util.ArrayList;
import java.util.List;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

public class ObjetoInvalido {
	String field;
	List<CamposInvalidos> camposInvalidos;
	
	public ObjetoInvalido(String field, ConstraintViolationException camposInvalidosInfo) {
		this.field = field;
		mapearExcepcion(camposInvalidosInfo);
	}

	public String getField() {
		return field;
	}

	public void setField(String field) {
		this.field = field;
	}

	public List<CamposInvalidos> getCamposInvalidos() {
		return camposInvalidos;
	}

	public void setCamposInvalidos(List<CamposInvalidos> camposInvalidos) {
		this.camposInvalidos = camposInvalidos;
	}
	
	private void mapearExcepcion(ConstraintViolationException ex) {
		camposInvalidos = new ArrayList<CamposInvalidos>();
		for (ConstraintViolation<?> i : ex.getConstraintViolations()) {
			camposInvalidos.add(new CamposInvalidos(i.getPropertyPath().toString(), i.getMessage()));
		}
	}
	
	
}
