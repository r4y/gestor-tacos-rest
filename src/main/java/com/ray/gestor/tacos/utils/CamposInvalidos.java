package com.ray.gestor.tacos.utils;

public class CamposInvalidos {
	private String campoInvalido;
	private String motivo;

	public CamposInvalidos(String campoInvalido, String motivo) {
		this.campoInvalido = campoInvalido;
		this.motivo = motivo;
	}

	public String getCampoInvalido() {
		return campoInvalido;
	}

	public void setCampoInvalido(String campoInvalido) {
		this.campoInvalido = campoInvalido;
	}

	public String getMotivo() {
		return motivo;
	}

	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}

	@Override
	public String toString() {
		return "CamposInvalidos [campoInvalido=" + campoInvalido + ", motivo=" + motivo + "]";
	}

}
