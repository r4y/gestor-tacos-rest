package com.ray.gestor.tacos.utils;

import java.sql.Timestamp;
import java.util.Date;

import org.springframework.http.HttpStatus;

public class RespuestaError <T> {
	private Date timestamp;
	private int status;
	private String error;
	private T message;
	private String path;

	public RespuestaError(HttpStatus httpStatus, T message, String path) {
		this.timestamp = new Timestamp(System.currentTimeMillis());
		this.status = httpStatus.value();
		this.error = httpStatus.getReasonPhrase();
		this.message = message;
		this.path = path;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public T getMessage() {
		return message;
	}

	public void setMessage(T message) {
		this.message = message;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	@Override
	public String toString() {
		return "RespuestaError [timestamp=" + timestamp + ", status=" + status + ", error=" + error + ", message="
				+ message + ", path=" + path + "]";
	}
	
	

}
